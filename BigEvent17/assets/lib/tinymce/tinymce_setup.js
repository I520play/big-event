function initEditor() {
  // tinymce 就是 tinymce.min.js 中提供的对象
  //  init 方法 就是在 设置 富文本编辑器 参数
  tinymce.init({
    //选择class为content的标签作为编辑器
    selector: '#txtContent',
    //方向从左到右
    directionality: 'ltr',
    //语言选择中文
    language: 'zh_CN',
    //高度为300
    height: 300,
    statusbar: true,
    width: '100%',
    //工具栏上面的补丁按钮
    plugins: [
      'advlist autolink link image lists charmap preview hr anchor pagebreak spellchecker',
      'searchreplace wordcount visualblocks visualchars code insertdatetime nonbreaking',
      'save table contextmenu directionality template paste textcolor',
      'codesample imageupload'
    ],
    //工具栏的补丁按钮
    toolbar:
      'insertfile undo redo | \
       styleselect | \
       bold italic | \
       alignleft aligncenter alignright alignjustify | \
       bullist numlist outdent indent | \
       image | \
       preview | \
       forecolor emoticons |\
       codesample fontsizeselect |\
       imageupload',
    //字体大小
    fontsize_formats: '10pt 12pt 14pt 18pt 24pt 36pt',
    //按tab不换行
    nonbreaking_force_tab: true
    //   imageupload_url: '/user/submit-image'
  })
}
