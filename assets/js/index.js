 //根据token查询当前登录用户的信息
 //getUserInfo()这个方法就是处理用户信息的包括头像和用户名
 function getUserInfo() {
    //发送ajax请求，获取数据，需要在请求头中携带token
    //只要发送ajax请求，就要先调用预处理过滤器（baseAPI.js）
    $.ajax({
        method: 'get',
        url: '/my/userinfo',
        // headers: {
        //     AUthorization: sessionStorage.getItem('token') || ''

        // },
        success: function (res) {
            //如果成功
            if (res.status === 0) {
                renderAvator(res.data)
            }
        }
    })
}
function renderAvator(unifo) {
    //显示欢迎
    //获取用户名
    let uName = unifo.nickname || unifo.username
    //设置用户名
    $('#welcome').text('欢迎' + uName)
    //显示图片,先判断图片头像是否存在
    if (unifo.user_pic) {
        $('.layui-nav-img').attr('src', unifo.user_pic).show()
        $('.text-avatar').hide()
    } else {
        //显示文字头像
        let first = uName[0].toUpperCase()
        $('.layui-nav-img').hide()
        $('.text-avatar').text(first).show()
    }
}


// 入口函数
$(function () {
    getUserInfo()
    $('#btnLogout').on('click',function () {
        //用户提示确认
        let index=layui.layer.confirm('确定退出吗',function(){
            //删除token
            sessionStorage.removeItem('token')
            //跳转页面
            location.replace('/login.html')
            //关闭当前窗口
            layui.layer.close(index)
        })
    })
})