
$(function () {
  // 1.添加 自定义 表单校验规则 --------------------
  layui.form.verify({
    nickname: [/^[\S]{1,6}$/, '密码必须在1到6位'],
  })

  // 2.定义获取用户信息的方法 ----------------------
  getUserInfo()
    //根据token查询当前登录用户的信息
    function getUserInfo() {
        //发送ajax请求，获取数据，需要在请求头中携带token
        //只要发送ajax请求，就要先调用预处理过滤器（baseAPI.js）
        $.ajax({
            method: 'get',
            url: '/my/userinfo',
            success: function (res) {
                //如果成功
                if (res.status === 0) {
                    layui.form.val('userForm',res.data)
                }
            }
        })
    }

  // 3.重置按钮 -----------------------------------
    $('#btnReset').on('click',getUserInfo())

  // 4.为表单 注册 提交事件 -------------------------
    $('.layui-form').on('submit',function(e){
      e.preventDefault()
      //获取表单数据(自动获取表单里面带有name属性的内容（键值对）)
      let data=layui.form.val('userForm')
      $.ajax({
        method:'post',
        url:'/my/userinfo',
        data:data,
        success(res){
          if(res.status===0){
            layui.layer.msg(res.message)
            window.parent.getUserInfo()
          }
        }
      })
    })
})