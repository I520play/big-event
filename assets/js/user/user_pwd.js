$(function () {
  //1.注册 自定义 校验规则 ------------------------------

  layui.form.verify({
    pwd: [/^[\S]{6,12}$/, '亲，密码必须在6-12位哦~~ : )'],
    // 新旧密码必须不一样 规则
    samepwd(newPwd) {
      if (newPwd === $('input[name=oldPwd]').val().trim()) {
        return '对不起，新旧密码不能一样 哦~~！'
      }
    },
    confirmpwd(confirmPwd) {
      if (confirmPwd !== $('input[name=newPwd]').val().trim()) {
        return '对不起，两次密码输入不一致哦~~'
      }
    }
  })

  //2.为表单注册提交事件 ------------------------------
  $('.layui-form').on('submit',function(e){
    e.preventDefault()
    //获取表单数据(自动获取表单里面带有name属性的内容（键值对）)
    let data=layui.form.val('userForm')
    $.ajax({
      method:'post',
      url:'/my/updatepwd',
      data:data,
      success(res){
        if(res.status===0){
          //提示信息
          layui.layer.msg(res.message,{icon:-1,time:1000},function(){
            //跳到登录页面
            window.top.location.replace('/login.html')
            //删除本地token
            sessionStorage.removeItem('token')
          })
        }
        else{
          //如果修改密码失败，提示错误消息
          layui.layer.msg(res.message)
        }
      }
    })
  })
})