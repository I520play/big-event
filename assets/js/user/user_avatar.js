$(function () {

  // 1.1 获取裁剪区域的 DOM 元素
  let $image = $('#image')
  // 1.2 配置选项
  const options = {
    // 纵横比
    aspectRatio: 1,
    // 指定预览区域
    preview: '.img-preview'
  }

  // 1.3 创建裁剪区域
  $image.cropper(options)

  // 2.上传按钮 --------------------------------------------
  $('#btnCho').on('click', function () {
    //点击触发点击
    $('#fileCho').trigger('click')
  })

  // 3.文件选择框 change事件----------------------------------
  // 触发时机：当 选中的文件 发生改变时 触发 
  $('#fileCho').on('change', function (e) {
    //a.判断为空
    if (e.target.files.length === 0) {
      return layui.layer.msg('请选择一张图片')
    }
    //b.将选中的图片转为虚拟url
    let f = e.target.files[0]
    let imgURL = URL.createObjectURL(f)
    console.log(imgURL);
    //c.设置给剪裁组件
    $image
      .cropper('destroy')//销毁旧的剪裁区域
      .attr('src', imgURL)//重新设置路径
      .cropper(options)//重新初始化剪裁区域
  })

  // 4.确定上传按钮 --------------------------------------------
  $('#btnYes').on('click',function(){
    //a.获取剪裁区域的图片数据
    let dataURL = $image
    .cropper('getCroppedCanvas', {
      // 创建一个 Canvas 画布
      width: 100,
      height: 100
    })
    .toDataURL('image/png')

    //b.将数据提交到服务器接口
    $.ajax({
      method: 'post',
      url: '/my/update/avatar',
      data:{
        avatar:dataURL
      },
      success: function (res) {
          //如果成功
          if (res.status === 0) {
            //提示成功消息
            layui.layer.msg(res.message)
            //通过调用getUserInfo()方法,重新请求用户信息,更新头像
            window.parent.getUserInfo()
          }
        
      }
  })
  })

})