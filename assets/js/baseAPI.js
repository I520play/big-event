// ajaxPrefilter 方法 注册了一个回调函数
//     此回调函数 会在 使用 jq的 异步请求方法时，执行
//     作用：在发送异步请求之前，修改 ajax配置对象
$.ajaxPrefilter((opt) => {
  //为ajax请求添加基地地址
  opt.url = 'http://api-breakingnews-web.itheima.net' + opt.url
  //相当于先点击登录执行了一次api/login登录页面拿token请求，然后在另一个网页再执行/my/请求，如果强行先执行/my/就登录不进去
  if (opt.url.includes('/my/')===true) {
    //检查本地有没有token,如果没有就直接返回登录页面
    if (!sessionStorage.getItem('token')) {
      alert('没有登录就进不来哟')
      location.replace('/login.html')
    }
    else {
      //如果有token 就拿到token，就能进去了
      opt.headers = {
        AUthorization: sessionStorage.getItem('token')
      }
    }
  }
  opt.complete = function (res) {
    if (res.responseJSON.status === 1 && res.responseJSON.message === '身份认证失败！') {
      //显示返回信息
      layui.layer.msg(res.responseJSON.message, {
        icon: 2,
        timer: 2000
      }, function () {
        //跳转登录页面
        location.replace('./login.html')
        //清空失效或者伪造的token
        localStorage.removeItem('token')
      })
    }
  }
})


