// 入口函数 -- dom树创建完毕就执行
$(function () {

  // 1.为按钮添加点击事件 ------------------------
  $('#link_reg').on('click', function () {
    $('.login-box').hide()
    $('.reg-box').show()
  })

  $('#link_login').on('click', function () {
    $('.login-box').show()
    $('.reg-box').hide()
  })

  // 2.为layui添加表单自定义规则 -------------------
  layui.form.verify({
    pwd: [/^[\S]{6,12}$/, '密码必须在6到12位'],
    repwd: function (value) {
      if (value !== $('#txtPwd').val())
        return '输入的不一样'
    }
  })

  // 3.注册表单提交事件 ----------------------------
  // 注意：当 layui 校验通过后，会提交表单，触发表单的提交事件
  $('#formReg').on('submit', function (e) {
    //阻断默认提交事件
    e.preventDefault()
    //获取表单要提交的数据
    let data = {
      username: $('#txtUName').val().trim(),
      password: $('#txtPwd').val().trim(),
    }
    //提交数据到接口
    $.post('/api/reguser', data, function (res) {
      //如果注册失败。则阻止函数运行并提示
      if (res.status !== 0) return layui.layer.msg(res.message)
      //如果成功，也提示
      layui.layer.msg(res.message)
      //模拟登录，按钮被点击
      $('#link_login').trigger('click')
      //将用户名自动填充到登录框 ，文本框
      $('#usrName').val(data.username)
      //清空表单
      $('#formReg')[0].reset()
    })
  })

  // 4.登录表单的提交事件 -------------------------
  $('#formLogin').on('submit', function (e) {
    //阻断默认提交事件
    e.preventDefault()
    //获取表单元素用户名和密码
    let data = layui.form.val('f1')
    //提交数据到接口
    $.post('/api/login', data, function (res) {
      if (res.status!= 0) return layui.layer.msg(res.message)
      layui.layer.msg(res.message, {
        icon: 1,
        time: 2000
      },function () {
          sessionStorage.setItem('token', res.token)
          location.replace('/index.html')
        })
    })
  })
})